# Maintainer: Yassine Oudjana (Tooniis) <y.oudjana@protonmail.com>
# Kernel config based on: arch/arm64/configs/defconfig

_flavor="postmarketos-qcom-msm8996"
pkgname=linux-$_flavor
pkgver=5.14_rc4
pkgrel=0
pkgdesc="Kernel close to mainline with extra patches for Qualcomm MSM8996 devices"
arch="aarch64"
_carch="arm64"
url="https://gitlab.com/msm8996-mainline/linux-msm8996"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native pmb:kconfigcheck-nftables"
makedepends="bison findutils flex installkernel openssl-dev perl"

# Source
_tag=v${pkgver//_/-}-msm8996
source="
	linux-msm8996-$_tag.tar.gz::$url/-/archive/$_tag/linux-msm8996-$_tag.tar.gz
	config-$_flavor.$arch
"
builddir="$srcdir/linux-msm8996-$_tag"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$CARCH" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION=$((pkgrel + 1 ))
}

package() {
	mkdir -p "$pkgdir"/boot
	install -Dm644 "$builddir/arch/$_carch/boot/Image.gz" \
		"$pkgdir/boot/vmlinuz-$_flavor"

	install -D "$builddir/include/config/kernel.release" \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"

	make modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir"/usr/share/dtb
}
sha512sums="
effd2fa2bbc840cc87330b66d86ca4a044a7394399bfe31f59e3d715ee4b704f4877532661fbdeb77d906a35845acaa1f0b4bd04771cd0ea417a384bf172608f  linux-msm8996-v5.14-rc4-msm8996.tar.gz
7870093b7f14b6b52fb1b1f7a1947cc96a90e63dea10a2f671ea97c5dfc9207ba17aea656ebd0aa982a303c817b34ac2592b8ae5e775b02f6348cbec0a6ec812  config-postmarketos-qcom-msm8996.aarch64
"
